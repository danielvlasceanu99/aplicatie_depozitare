﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project___Activitatea_de_aprovizionare
{
    public class Furnizori
    {   
        public int idFurnizor;
        public string numeFurnizor;
        public string adresa;
        public string nrTelefon;
        public string email;
        public List<string> listaMateriale;
        public static int numarFurnizori = 1;
        
        public Furnizori() { idFurnizor = numarFurnizori++; }
        public Furnizori(string numeFurnizor, string adresa, string nrTelefon, string email, List<string> listaMateriale)
        {
            idFurnizor = numarFurnizori++;
            this.numeFurnizor = numeFurnizor;
            this.adresa = adresa;
            this.nrTelefon = nrTelefon;
            this.email = email;
            this.listaMateriale = listaMateriale;
        }
    }
}