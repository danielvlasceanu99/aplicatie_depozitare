﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Add_Contract : Form
    {
        // Create the list;
        List<object> lista = new List<object>();
        int nrLinii = 1;
        int distanta = 25;
        Furnizori auxFurnizor;
        Contracte auxContracte;

        public Form_Add_Contract(Furnizori pFurnizor, Contracte pContract)
        {
            InitializeComponent();
            lblTitle.Text += ": " + pContract.nrContract.ToString();

            // We copyy the parameters to work with them later]
            auxFurnizor = pFurnizor;
            auxContracte = pContract;

            // Create the first line and add it to the list;
            linie l1 = new linie();
            l1.butonSterge = btnSterge1;
            l1.material = txtMaterial1;
            lista.Add(l1);
        }
        private void BtnNewLine_Click(object sender, EventArgs e)
        {
            // Initialize line components
            Button btnNewSterge = new Button();
            TextBox txtNewMaterial = new TextBox();
            btnNewLine.Enabled = false;

            // New button settings
            btnNewSterge.Location = new Point(btnSterge1.Location.X, btnSterge1.Location.Y + nrLinii * distanta);
            btnNewSterge.Name = "btnSterge" + (nrLinii + 1).ToString();
            btnNewSterge.Size = btnSterge1.Size;
            btnNewSterge.TabIndex = btnSterge1.TabIndex + 2 * nrLinii - 1;
            btnNewSterge.Text = btnSterge1.Text;
            btnNewSterge.Click += new EventHandler(BtnSterge1_Click);

            // New text box components
            txtNewMaterial.Location = new Point(txtMaterial1.Location.X, txtMaterial1.Location.Y + nrLinii * distanta);
            txtNewMaterial.Name = "txtMaterial" + (nrLinii + 1).ToString();
            txtNewMaterial.Size = txtMaterial1.Size;
            txtNewMaterial.TabIndex = txtMaterial1.TabIndex + 2 * nrLinii - 1;

            // Add elements to the current form;
            Controls.Add(btnNewSterge);
            Controls.Add(txtNewMaterial);

            // Create the line and add the elements to it;
            linie newLine = new linie();
            newLine.butonSterge = btnNewSterge;
            newLine.material = txtNewMaterial;
            lista.Add(newLine);

            // Change the position for the newline button;
            btnNewLine.Location = new Point(btnNewLine.Location.X,  btnSterge1.Location.Y + nrLinii * distanta);
            btnNewLine.TabIndex = btnNewLine.TabIndex + 2 * nrLinii;
            btnSave.TabIndex = btnNewLine.TabIndex + 1;
            nrLinii++;
        }
        private void BtnSterge1_Click(object sender, EventArgs e)
        {
            // Find the line that will be deleted and save it;
            linie linieNow = (linie)lista.Find(m => ((linie)m).butonSterge == (Button)sender);
            int index = lista.IndexOf(linieNow);

            // For every line under the one that will be deleted, move them one line up;
            for(int i = index + 1; i<lista.Count; i++)
            {
                // Move btnStergere;
                ((Button)((linie)lista[i]).butonSterge).Location = new Point(
                    ((Button)((linie)lista[i]).butonSterge).Location.X,
                    ((Button)((linie)lista[i]).butonSterge).Location.Y - distanta);

                // Move txtMaterial;
                ((TextBox)((linie)lista[i]).material).Location = new Point(
                    ((TextBox)((linie)lista[i]).material).Location.X,
                    ((TextBox)((linie)lista[i]).material).Location.Y - distanta);
            }
            // Remove the line from the form and move the newLine button;
            Controls.Remove((Button)linieNow.butonSterge);
            Controls.Remove((TextBox)linieNow.material);
            btnNewLine.Location = new Point(btnNewLine.Location.X, btnNewLine.Location.Y - distanta);
            btnNewLine.Enabled = true;
            // Remove the line from the list;
            lista.Remove(linieNow);
            nrLinii--;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            // Initialize the objects that will go back to Form1;
            // Object: Furnizor
            bool err = false;
            List<object> aux = new List<object>(); ;
            foreach(object o in lista)
            {
                aux.Add(o);
            }

            if (txtFurnizor.Text.Length != 0)
            {
                auxFurnizor.numeFurnizor = txtFurnizor.Text;
            }
            else
            {
                MessageBox.Show("Nu ati introdus numele furnizorului.");
                err = true;
            }
            if (txtAdresa.Text.Length != 0)
            {
                auxFurnizor.adresa = txtAdresa.Text;
            }
            else
            {
                MessageBox.Show("Nu ati introdus adresa.");
                err = true;
            }
            if (txtTelefon.Text.Length == 10)
            {
                auxFurnizor.nrTelefon = txtTelefon.Text;
            }
            else
            {
                MessageBox.Show("Nu ati introdus telefonul.");
                err = true;
            }
            if (txtEmail.Text.Length != 0)
            {
                auxFurnizor.email = txtEmail.Text;
            }
            else
            {
                MessageBox.Show("Nu ati introdus adresa de email.");
                err = true;
            }
            auxFurnizor.listaMateriale = new List<string>();
            if (aux.Count > 0)
            {
                if (((TextBox)((linie)lista[nrLinii - 1]).material).Text.Length == 0)
                {
                    aux.RemoveAt(nrLinii - 1);
                }
            }
            for (int i = 0; i <aux.Count; i++)
            {
                string auxMaterial = ((TextBox)((linie)lista[i]).material).Text;
                auxFurnizor.listaMateriale.Add(auxMaterial);
            }
            // Object: Contract
            auxContracte.furnizor = auxFurnizor;
            auxContracte.dataContract = DateTime.Now;
            
            if (aux.Count == 0 )
            {
                MessageBox.Show("Nu ati introdus materialele furnizate.");
                err = true;
            }
            if(err == false)
            {
                DialogResult = DialogResult.OK;
                MessageBox.Show("Inregisrare reusita.");
            }
            else
            {
                auxFurnizor.listaMateriale.Clear();
            }
        }
        private void TxtMaterial1_TextChanged(object sender, EventArgs e)
        {
            btnNewLine.Enabled = true;
        }
    }
    public class linie
    {
        public object material;
        public object butonSterge;
    }
}