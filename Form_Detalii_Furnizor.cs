﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Detalii_Furnizor : Form
    {
        Furnizori auxFurnizor = new Furnizori();

        public Form_Detalii_Furnizor(Furnizori pFurnizori)
        {
            InitializeComponent();
            auxFurnizor = pFurnizori;

            // Write the elements from the parameter inside the form;
            lblTitle.Text = auxFurnizor.numeFurnizor;
            lblIdFurnizor.Text = lblIdFurnizor.Text + auxFurnizor.idFurnizor.ToString();
            txtAdresa.Text = auxFurnizor.adresa;
            txtTelefon.Text = auxFurnizor.nrTelefon;
            txtEmail.Text = auxFurnizor.email;

            // Write the material list
            txtMaterial1.Text = auxFurnizor.listaMateriale[0];
            for(int i = 1; i< auxFurnizor.listaMateriale.Count; i++)
            {
                TextBox txtMaterial = new TextBox();
                txtMaterial.Enabled = false;
                txtMaterial.Location = new Point(txtMaterial1.Location.X, txtMaterial1.Location.Y + i * 25);
                txtMaterial.Name = "txtMaterial" + (i + 1).ToString();
                txtMaterial.Size = txtMaterial1.Size;
                txtMaterial.TabIndex = txtMaterial1.TabIndex + i;
                txtMaterial.Text = auxFurnizor.listaMateriale[i];

                Controls.Add(txtMaterial);

                btnExit.Location = new Point(btnExit.Location.X, btnExit.Location.Y + 25);
                btnSave.Location = new Point(btnSave.Location.X, btnSave.Location.Y + 25);
            }
        }
        private void BtnModifica_Click(object sender, EventArgs e)
        {
            txtAdresa.Enabled = true;
            txtTelefon.Enabled = true;
            txtEmail.Enabled = true;
            btnSave.Enabled = true;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            bool err = false;
            if (txtAdresa.Text.Length >= 3) { auxFurnizor.adresa = txtAdresa.Text;}
            else { MessageBox.Show("Adresa Invalida !"); err = true; }

            if (txtTelefon.Text.Length == 10) { auxFurnizor.nrTelefon = txtTelefon.Text; }
            else { MessageBox.Show("Numar de telefon invalid !"); err = true; }

            if (txtEmail.Text.Length >= 7) { auxFurnizor.email = txtEmail.Text;  }
            else { MessageBox.Show("Email invalid !"); err = true; }

            if(err == false)
            {
                txtAdresa.Enabled = false;
                txtTelefon.Enabled = false;
                txtEmail.Enabled = false;
            }
        }
        private void BtnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}