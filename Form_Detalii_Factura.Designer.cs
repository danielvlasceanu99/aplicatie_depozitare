﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Detalii_Factura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTotalText = new System.Windows.Forms.Label();
            this.lblValoare = new System.Windows.Forms.Label();
            this.txtMaterial1 = new System.Windows.Forms.TextBox();
            this.txtPret1 = new System.Windows.Forms.TextBox();
            this.txtCantitate1 = new System.Windows.Forms.TextBox();
            this.txtSubtotal1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblTitle.Location = new System.Drawing.Point(18, 11);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(154, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Factura NR. ";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.lblData.Location = new System.Drawing.Point(19, 50);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(54, 25);
            this.lblData.TabIndex = 1;
            this.lblData.Text = "Data";
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblClient.Location = new System.Drawing.Point(20, 97);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(58, 22);
            this.lblClient.TabIndex = 2;
            this.lblClient.Text = "Client";
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.AutoSize = true;
            this.lblSubtitle.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.lblSubtitle.Location = new System.Drawing.Point(19, 153);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.Size = new System.Drawing.Size(266, 27);
            this.lblSubtitle.TabIndex = 3;
            this.lblSubtitle.Text = "Lista materiale comandate";
            // 
            // lblTotalText
            // 
            this.lblTotalText.AutoSize = true;
            this.lblTotalText.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTotalText.Location = new System.Drawing.Point(12, 604);
            this.lblTotalText.Name = "lblTotalText";
            this.lblTotalText.Size = new System.Drawing.Size(51, 22);
            this.lblTotalText.TabIndex = 4;
            this.lblTotalText.Text = "Total";
            // 
            // lblValoare
            // 
            this.lblValoare.AutoSize = true;
            this.lblValoare.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblValoare.Location = new System.Drawing.Point(350, 604);
            this.lblValoare.Name = "lblValoare";
            this.lblValoare.Size = new System.Drawing.Size(71, 22);
            this.lblValoare.TabIndex = 9;
            this.lblValoare.Text = "Valoare";
            // 
            // txtMaterial1
            // 
            this.txtMaterial1.Enabled = false;
            this.txtMaterial1.Location = new System.Drawing.Point(24, 183);
            this.txtMaterial1.Name = "txtMaterial1";
            this.txtMaterial1.Size = new System.Drawing.Size(297, 22);
            this.txtMaterial1.TabIndex = 10;
            // 
            // txtPret1
            // 
            this.txtPret1.Enabled = false;
            this.txtPret1.Location = new System.Drawing.Point(327, 183);
            this.txtPret1.Name = "txtPret1";
            this.txtPret1.Size = new System.Drawing.Size(48, 22);
            this.txtPret1.TabIndex = 11;
            // 
            // txtCantitate1
            // 
            this.txtCantitate1.Enabled = false;
            this.txtCantitate1.Location = new System.Drawing.Point(381, 183);
            this.txtCantitate1.Name = "txtCantitate1";
            this.txtCantitate1.Size = new System.Drawing.Size(40, 22);
            this.txtCantitate1.TabIndex = 12;
            // 
            // txtSubtotal1
            // 
            this.txtSubtotal1.Enabled = false;
            this.txtSubtotal1.Location = new System.Drawing.Point(327, 211);
            this.txtSubtotal1.Name = "txtSubtotal1";
            this.txtSubtotal1.Size = new System.Drawing.Size(94, 22);
            this.txtSubtotal1.TabIndex = 13;
            // 
            // Form_Detalii_Factura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(430, 635);
            this.Controls.Add(this.txtSubtotal1);
            this.Controls.Add(this.txtCantitate1);
            this.Controls.Add(this.txtPret1);
            this.Controls.Add(this.txtMaterial1);
            this.Controls.Add(this.lblValoare);
            this.Controls.Add(this.lblTotalText);
            this.Controls.Add(this.lblSubtitle);
            this.Controls.Add(this.lblClient);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Detalii_Factura";
            this.Text = "Form_Detalii_Factura";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblSubtitle;
        private System.Windows.Forms.Label lblTotalText;
        private System.Windows.Forms.Label lblValoare;
        private System.Windows.Forms.TextBox txtMaterial1;
        private System.Windows.Forms.TextBox txtPret1;
        private System.Windows.Forms.TextBox txtCantitate1;
        private System.Windows.Forms.TextBox txtSubtotal1;
    }
}