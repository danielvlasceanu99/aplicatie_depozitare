﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project___Activitatea_de_aprovizionare
{
    public class Materiale
    {
        public int idMaterial;
        public string denumireMaterial;
        public string furnizor;
        public int stocCurent;
        public double pret;
        public static int numarMateriale = 1;

        public Materiale() { idMaterial = numarMateriale++; }
        public Materiale(string denumireMaterial, string furnizor, int stocCurent, double pret)
        {
            this.idMaterial = numarMateriale++;
            this.denumireMaterial = denumireMaterial;
            this.furnizor = furnizor;
            this.stocCurent = stocCurent;
            this.pret = pret;
        }

        public override string ToString()
        {
            return idMaterial.ToString() + "," + denumireMaterial + "," + furnizor + "," + stocCurent.ToString() + "," + pret.ToString();
        }
    }
}
