﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Add_Factura : Form
    {
        List<linie2> lista = new List<linie2>();
        List<Materiale> auxListMateriale;
        List<string> auxMateriale = new List<string>();
        Facturi auxFactura;
        int nrLinii = 1;
        int distanta = 25;
        double valoare = 0;
        double Total = 0;

        public Form_Add_Factura(Facturi pFactura, List<Materiale> pListMateriale)
        {
            InitializeComponent();
            auxFactura = pFactura;
            auxListMateriale = pListMateriale;

            lblTitle.Text += auxFactura.nrFactura.ToString();

            foreach (Materiale m in auxListMateriale)
            {
                auxMateriale.Add(m.denumireMaterial);
            }
            foreach (string m in auxMateriale)
            {
                cbMaterial.Items.Add(m);
            }

            linie2 newLine = new linie2();
            newLine.btnDelete = btnDelete;
            newLine.cbMaterial = cbMaterial;
            newLine.txtCantitate = txtCantitate;
            lista.Add(newLine);
        }
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Button delete = new Button();
            ComboBox materiale = new ComboBox();
            TextBox cantitate = new TextBox();
            btnAdd.Enabled = false;

            if (nrLinii > 0)
            {
                string item = ((ComboBox)lista[nrLinii - 1].cbMaterial).SelectedItem.ToString();
                auxMateriale.Remove(item);
            }

            delete.Location = new Point(btnDelete.Location.X, btnDelete.Location.Y + nrLinii * distanta);
            delete.Name = "btnSterge" + (nrLinii + 1).ToString();
            delete.Size = btnDelete.Size;
            delete.TabIndex = btnDelete.TabIndex + 2 * nrLinii;
            delete.Text = btnDelete.Text;
            delete.Click += new EventHandler(BtnDelete_Click);

            materiale.Location = new Point(cbMaterial.Location.X, cbMaterial.Location.Y + nrLinii * distanta);
            materiale.Name = "cbMaterial" + (nrLinii + 1).ToString();
            materiale.Size = cbMaterial.Size;
            materiale.TabIndex = cbMaterial.TabIndex + 2 * nrLinii;
            materiale.SelectedIndexChanged += new EventHandler(CbMaterial_SelectedIndexChanged);
            foreach (string m in auxMateriale)
            {
                materiale.Items.Add(m);
            }

            cantitate.Enabled = false;
            cantitate.Location = new Point(txtCantitate.Location.X, txtCantitate.Location.Y + nrLinii * distanta);
            cantitate.Name = "txtCantitate" + (nrLinii + 1).ToString();
            cantitate.Size = txtCantitate.Size;
            cantitate.TabIndex = txtCantitate.TabIndex + 2 * nrLinii;
            cantitate.TextChanged += new EventHandler(TxtCantitate_TextChanged);

            Controls.Add(delete);
            Controls.Add(materiale);
            Controls.Add(cantitate);

            linie2 newLine = new linie2();
            newLine.btnDelete = delete;
            newLine.cbMaterial = materiale;
            newLine.txtCantitate = cantitate;
            lista.Add(newLine);
            btnAdd.Location = new Point(btnAdd.Location.X, btnAdd.Location.Y + distanta);

            nrLinii++;
            Total += valoare;
            valoare = 0;
            lblTotal.Text = "Total: " + Total.ToString();
        }
        private void CbMaterial_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            TextBox box = (TextBox)lista.Find(m => m.cbMaterial == sender).txtCantitate;
            box.Enabled = true;
            box.Text = "1";
            //box.SelectAll();
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            linie2 linieNow = lista.Find(m => (m).btnDelete == (Button)sender);
            int index = lista.IndexOf(linieNow);
            for (int i = index + 1; i < lista.Count; i++)
            {
                ((Button)lista[i].btnDelete).Location = new Point(
                    ((Button)lista[i].btnDelete).Location.X,
                    ((Button)(lista[i]).btnDelete).Location.Y - distanta);

                ((ComboBox)lista[i].cbMaterial).Location = new Point(
                    ((ComboBox)lista[i].cbMaterial).Location.X,
                    ((ComboBox)lista[i].cbMaterial).Location.Y - distanta);

                ((TextBox)lista[i].txtCantitate).Location = new Point(
                   ((TextBox)lista[i].txtCantitate).Location.X,
                   ((TextBox)lista[i].txtCantitate).Location.Y - distanta);
            }
            Controls.Remove((Button)linieNow.btnDelete);
            Controls.Remove((ComboBox)linieNow.cbMaterial);
            try
            {
                Total -= Convert.ToDouble(linieNow.txtCantitate);
            }
            catch(Exception ex) { }
            Controls.Remove((TextBox)linieNow.txtCantitate);
            if (((ComboBox)linieNow.cbMaterial).SelectedIndex > -1)
            {
                updateList(((ComboBox)linieNow.cbMaterial).SelectedItem.ToString());
            }
            btnAdd.Location = new Point(btnAdd.Location.X, btnAdd.Location.Y - distanta);
            lblTotal.Text = "Total: " + Total.ToString();
            lista.Remove(linieNow);
            nrLinii--;
            btnAdd.Enabled = true;
        }
        private void updateList(string item)
        {
            List<string> aux = new List<string>();
            foreach(Materiale m in auxListMateriale)
            {
                if(auxMateriale.IndexOf(m.denumireMaterial) > -1 || m.denumireMaterial == item)
                {
                    aux.Add(m.denumireMaterial);
                }
            }
            auxMateriale.Clear();
            foreach(string m in aux)
            {
                auxMateriale.Add(m);
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            bool err = false;
            if (lista.Count > 0)
            {
                if (((ComboBox)lista[nrLinii - 1].cbMaterial).SelectedIndex == -1)
                {
                    lista.RemoveAt(nrLinii - 1);
                }
            }
            if(txtClient.Text.Length != 0) { auxFactura.client = txtClient.Text; }
            else
            {
                MessageBox.Show("Nu ati introdus clientul");
                err = true;
            }
            auxFactura.dataFactura = DateTime.Now;
            auxFactura.valoare = 0;
            for(int i = 0; i <lista.Count; i++)
            {
                string nume = ((ComboBox)lista[i].cbMaterial).SelectedItem.ToString();
                Materiale x = auxListMateriale.Find(m => m.denumireMaterial == nume);
                double pret = x.pret;
                int cantitate = Convert.ToInt32(((TextBox)lista[i].txtCantitate).Text);
                x.stocCurent -= cantitate;
                auxFactura.valoare += pret * cantitate;
                auxFactura.cantitate.Add(cantitate);
                auxFactura.listaMateiale.Add(x);
            }
            if (lista.Count == 0)
            {
                MessageBox.Show("Nu ati ales materiale");
                err = true;
            }
            if(err == false)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                auxFactura.listaMateiale.Clear();
            }
        }
        private void TxtCantitate_TextChanged(object sender, EventArgs e)
        {
            string nume = ((ComboBox)lista.Find(m => m.txtCantitate == sender).cbMaterial).SelectedItem.ToString();
            int cant = auxListMateriale.Find(m => m.denumireMaterial == nume).stocCurent;
            int c;
            double pr = auxListMateriale.Find(m => m.denumireMaterial == nume).pret;
            try
            {
                c = Convert.ToInt32(((TextBox)sender).Text);
                if(c > cant)
                {
                    MessageBox.Show("Conversie invalida");
                    ((TextBox)sender).Text = cant.ToString();
                    valoare = cant * pr;
                }
                else
                {
                    ((TextBox)sender).Text = c.ToString();
                    valoare = c * pr;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Conversie invalida");
                ((TextBox)sender).Text = 1.ToString();
            }
        }
        private void BtnSubtotal_Click(object sender, EventArgs e)
        {
            Total += valoare;
            valoare = 0;
            lblTotal.Text = "Total: " + Total.ToString();
        }
    }
    public class linie2
    {
        public object btnDelete;
        public object cbMaterial;
        public object txtCantitate;
    }
}