﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Afiseaza_Contracte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvContracte = new System.Windows.Forms.ListView();
            this.Numar = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Furnizor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataContract = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvContracte
            // 
            this.lvContracte.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Numar,
            this.Furnizor,
            this.dataContract});
            this.lvContracte.HideSelection = false;
            this.lvContracte.Location = new System.Drawing.Point(15, 14);
            this.lvContracte.Name = "lvContracte";
            this.lvContracte.Size = new System.Drawing.Size(510, 161);
            this.lvContracte.TabIndex = 0;
            this.lvContracte.UseCompatibleStateImageBehavior = false;
            this.lvContracte.View = System.Windows.Forms.View.Details;
            // 
            // Numar
            // 
            this.Numar.Text = "Numar contract";
            this.Numar.Width = 110;
            // 
            // Furnizor
            // 
            this.Furnizor.Text = "Furnizor";
            this.Furnizor.Width = 120;
            // 
            // dataContract
            // 
            this.dataContract.Text = "Data contract";
            this.dataContract.Width = 150;
            // 
            // afiseazaContractForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 202);
            this.Controls.Add(this.lvContracte);
            this.Name = "afiseazaContractForm";
            this.Text = "AfiseazaContractForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvContracte;
        private System.Windows.Forms.ColumnHeader Numar;
        private System.Windows.Forms.ColumnHeader Furnizor;
        private System.Windows.Forms.ColumnHeader dataContract;
    }
}