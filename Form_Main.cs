﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Main : Form
    {
        List<Furnizori> listFurnizori = new List<Furnizori>();
        List<Materiale> listMateriale = new List<Materiale>();
        List<Facturi> listFacturi = new List<Facturi>();
        List<Contracte> listContracte = new List<Contracte>();
        Button back = new Button();

        public Form_Main()
        {
            InitializeComponent();
            lblHello.Text = "Neata utilizator !\nData de azi: " + DateTime.Now.DayOfWeek.ToString() + " ";
            lblHello.Text += DateTime.Now.Day.ToString() + ".";
            lblHello.Text += DateTime.Now.Month.ToString() + ".";
            lblHello.Text += DateTime.Now.Year.ToString() + ".";
            lblHello.Text += "\n\nPentru detalii despre modul de";
            lblHello.Text += "\nutilizare accesati meniul Help";
            testData();
        }
        private void InregistrareContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Furnizori pFurnizor = new Furnizori();
            Contracte pContract = new Contracte();
            Form_Add_Contract addContract = new Form_Add_Contract(pFurnizor, pContract);
            addContract.ShowDialog();
            if (addContract.DialogResult == DialogResult.OK)
            {
                listContracte.Add(pContract);
                listFurnizori.Add(pFurnizor);
            }
        }
        private void AfiseazaContracteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Afiseaza_Contracte afiseazaContracte = new Form_Afiseaza_Contracte(listContracte);
            afiseazaContracte.ShowDialog();
        }
        private void FurnizoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Afiseaza_Furnizori afiseazaFurnizori = new Form_Afiseaza_Furnizori(listFurnizori);
            afiseazaFurnizori.ShowDialog();
        }
        private void VizualizareStocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Afiseaza_Materiale afiseazaMateriale = new Form_Afiseaza_Materiale(listMateriale);
            afiseazaMateriale.ShowDialog();
        }
        private void AdaugaMaterialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materiale pMaterial = new Materiale("","",0,0);
            Form_Add_Material addMaterial = new Form_Add_Material(pMaterial, listFurnizori);
            addMaterial.ShowDialog();

            if(addMaterial.DialogResult == DialogResult.OK)
            {
                listMateriale.Add(pMaterial);
            }
        }
        private void IncarcatiInFisierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "CSV files (*csv)|*.csv";
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                FileStream file = new FileStream(saveFile.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter stream = new StreamWriter(file);
                foreach (Materiale index in listMateriale)
                {
                    stream.WriteLine(index.ToString());
                } 
                stream.Close();
                file.Close();
                MessageBox.Show("Incarcare efectuata cu succes !");
            }
        }
        private void VizualizareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Afiseaza_Facturi afiseazaFacturi = new Form_Afiseaza_Facturi(listFacturi);
            afiseazaFacturi.ShowDialog();
        }
        private void EmiteFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Facturi pFactura = new Facturi();
            Form_Add_Factura addFactura = new Form_Add_Factura(pFactura, listMateriale);
            addFactura.ShowDialog();
            if (addFactura.DialogResult == DialogResult.OK)
            {
                listFacturi.Add(pFactura);
            }
        }
        private void testData()
        {
            List<string> caramizi = new List<string> { "Caramizi verzi", "Caramizi albasre" };
            List<string> ciment = new List<string> { "Ciment roz", "Ciment verde" };
            List<string> scanduri = new List<string> { "Blana de urs", "Blana de castor" };

            Furnizori fCaramizi = new Furnizori("S.C. Caramizi S.R.L.", "Acasa ca e carantina", "0123456789", "caramizi@mail.com", caramizi);
            Furnizori fCiment = new Furnizori("S.C. Ciment S.R.L.", "Acasa ca e carantina", "0123456789", "ciment@mail.com", ciment);
            Furnizori fScanduri = new Furnizori("S.C. Scanduri S.R.L.", "Acasa ca e carantina", "0123456789", "scanduri@mail.com", scanduri);

            Contracte cCaramizi = new Contracte(fCaramizi, new DateTime(2011, 01, 29));
            Contracte cCiment = new Contracte(fCiment, new DateTime(2019, 12, 29));
            Contracte cScanduri = new Contracte(fScanduri, DateTime.Now);

            Materiale mCaramiziVerzi = new Materiale("Caramizi verzi", "S.C. Caramizi S.R.L.", 50, 8.6);
            Materiale mCaramizAlbastre = new Materiale("Caramizi albasre", "S.C. Caramizi S.R.L.", 80, 2.6);
            Materiale mCimentRoz = new Materiale("Ciment roz", "S.C.Ciment S.R.L.", 54, 1.5);
            Materiale mCimentVerde = new Materiale("Ciment verde", "S.C.Ciment S.R.L.", 59, 15.6);
            Materiale mBlanaUrs = new Materiale("Blana de urs", "S.C. Scanduri S.R.L.", 6, 11.9);
            Materiale mBlanaCastor = new Materiale("blana de castor", "Blana de castor", 15, 7.3);

            List<Materiale> auxListM1 = new List<Materiale> { mCaramiziVerzi, mCimentRoz };
            List<int> auxListC1 = new List<int> { 5, 6 };
            List<Materiale> auxListM2 = new List<Materiale> { mCimentVerde, mBlanaCastor };
            List<int> auxListC2 = new List<int> { 8, 15 };
            List<Materiale> auxListM3 = new List<Materiale> { mCaramizAlbastre, mBlanaUrs };
            List<int> auxListC3 = new List<int> { 25, 7 };

            Facturi fFactura1 = new Facturi(new DateTime(2019, 12, 25), "Gigel", auxListM1, auxListC1);
            Facturi fFactura2 = new Facturi(new DateTime(2020, 02, 25), "Vasile", auxListM2, auxListC2);
            Facturi fFactura3 = new Facturi(new DateTime(2019, 10, 03), "Petrica", auxListM3, auxListC3);

            listContracte.Add(cCaramizi);
            listContracte.Add(cCiment);
            listContracte.Add(cScanduri);

            listFurnizori.Add(fCaramizi);
            listFurnizori.Add(fCiment);
            listFurnizori.Add(fScanduri);

            listMateriale.Add(mCaramiziVerzi);
            listMateriale.Add(mCaramizAlbastre);
            listMateriale.Add(mCimentRoz);
            listMateriale.Add(mCimentVerde);
            listMateriale.Add(mBlanaUrs);
            listMateriale.Add(mBlanaCastor);

            listFacturi.Add(fFactura1);
            listFacturi.Add(fFactura2);
            listFacturi.Add(fFactura3);
        }
        private void closeHelp(object sender, EventArgs e)
        {
            lblHello.Font = new Font("Times New Roman", 16F);
            lblHello.Text = "";
            lblHello.Text += "Neata utilizator !\nData de azi: " + DateTime.Now.DayOfWeek.ToString() + " ";
            lblHello.Text += DateTime.Now.Day.ToString() + ".";
            lblHello.Text += DateTime.Now.Month.ToString() + ".";
            lblHello.Text += DateTime.Now.Year.ToString() + ".";
            lblHello.Text += "\n\nPentru detalii despre modul de";
            lblHello.Text += "\nutilizare accesati meniul Help";
            Controls.Remove(back);
        }
        private void FurnizoriToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try { Controls.Remove(back);}
            catch(Exception ex) { }
            lblHello.Font = new Font("Times New Roman", 12F);
            lblHello.Text = "";
            lblHello.Text += "Meniul Furnizori:\n";
            lblHello.Text += "Permite vizualizarea tuturor furnizorilor si \n";
            lblHello.Text += "a detaliilor despre un furnizor prin apasarea butonului\n";
            lblHello.Text += "'Detalii' de pe linia corespunzatoare. Datele despre un\n";
            lblHello.Text += "furnizor pot fi modificate apasand butonul 'Modifica'\n";
            lblHello.Text += "din fereastra detalii. Modificaarile se salveaza \n";
            lblHello.Text += "prin apasarea butonului 'Save' si sunt anulate prin'\n";
            lblHello.Text += "butonul 'Exit'\n";
            lblHello.Text += "\nAdaugarea unui nou furnizor se face prin adaugarea\n";
            lblHello.Text += "unui nou contract.";

            back.Font = new Font("Times New Roman", 12F);
            back.Location = new Point(12, 35);
            back.Name = "back";
            back.Text = "Back";
            back.Size = new Size(60, 30);
            back.Click += new EventHandler(closeHelp);
            Controls.Add(back);
        }
        private void StocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { Controls.Remove(back); }
            catch (Exception ex) { }
            lblHello.Font = new Font("Times New Roman", 12F);
            lblHello.Text = "";
            lblHello.Text += "Meniul Stoc:\n";
            lblHello.Text += "\nComanda 'Vizualizare stoc'\n";
            lblHello.Text += "Permite vizualizarea tuturor materialelor din stoc\n";
            lblHello.Text += "si modificarea stocului curent si al pretului prin\n";
            lblHello.Text += "apasarea butonului 'Modifica' si salvarea modificarilor\n";
            lblHello.Text += "cu ajutorul butonului 'Save'\n";
            lblHello.Text += "\nComanda 'Adauga material\n";
            lblHello.Text += "Permite adaugarea unui material in stoc introducand\n";
            lblHello.Text += "denumirea, pretul, furnizorul si apasand butonului 'Save'\n";
            lblHello.Text += "\nComanda 'Incarcati in fisier'\n";
            lblHello.Text += "Permite incarcarea tuturor materialelor din stoc intr-un\n";
            lblHello.Text += "fisier *.CSV\n";

            back.Font = new Font("Times New Roman", 12F);
            back.Location = new Point(8, 35);
            back.Name = "back";
            back.Text = "Back";
            back.Size = new Size(60, 30);
            back.Click += new EventHandler(closeHelp);
            Controls.Add(back);
        }
        private void FacturiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try { Controls.Remove(back); }
            catch (Exception ex) { }
            lblHello.Font = new Font("Times New Roman", 12F);
            lblHello.Text = "";
            lblHello.Text += "Meniul Facturi:\n";
            lblHello.Text += "\nComanda 'Vizualizare facturi'\n";
            lblHello.Text += "Permite vizualizarea tuturor facturilor emise\n";
            lblHello.Text += "a detaliilor pentu fiecare factura prin \n";
            lblHello.Text += "selectarea unei facturi + clic dreapta si ordonarea\n";
            lblHello.Text += "acestora dupa valoare sau id prin apasarea unei\n";
            lblHello.Text += "coloane din lista\n";
            lblHello.Text += "\nComanda 'Emite factura'\n";
            lblHello.Text += "Permite adaugarea unei facturi care va micsora stocul\n";
            lblHello.Text += "curent din fiecare material comandat, cantitatea maxima\n";
            lblHello.Text += "care poate fi comandata fiind stocul curent.\n";
            lblHello.Text += "Salvarea facturii se face prin apasarea butonului 'Save'\n";

            back.Font = new Font("Times New Roman", 12F);
            back.Location = new Point(8, 35);
            back.Name = "back";
            back.Text = "Back";
            back.Size = new Size(60, 30);
            back.Click += new EventHandler(closeHelp);
            Controls.Add(back);
        }
        private void ContracteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try { Controls.Remove(back); }
            catch (Exception ex) { }
            lblHello.Font = new Font("Times New Roman", 12F);
            lblHello.Text = "";
            lblHello.Text += "Meniul Contracte:\n";
            lblHello.Text += "\nComanda 'Vizualizeaza Contracte:\n";
            lblHello.Text += "Permite vizualizarea tuturor contractelor.\n";
            lblHello.Text += "\nComanda 'Inregistrare contract:\n";
            lblHello.Text += "Permite inregistrarea unui nou contract care va adauga\n";
            lblHello.Text += "si un furnizor.\n";
            lblHello.Text += "Salvarea se face prin butonul 'Save'\n";

            back.Font = new Font("Times New Roman", 12F);
            back.Location = new Point(12, 35);
            back.Name = "back";
            back.Text = "Back";
            back.Size = new Size(60, 30);
            back.Click += new EventHandler(closeHelp);
            Controls.Add(back);
        }
    }
}