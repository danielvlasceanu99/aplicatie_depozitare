﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project___Activitatea_de_aprovizionare
{
    public class Facturi: IComparable
    {
        public int nrFactura;
        public DateTime dataFactura;
        public double valoare;
        public string client;
        public List<Materiale> listaMateiale;
        public List<int> cantitate;
        public static int nrFacturi = 1;

        public Facturi()
        {
            nrFactura = nrFacturi++;
            cantitate = new List<int>();
            listaMateiale = new List<Materiale>();
        }
        public Facturi(DateTime dataFactura, string client, List<Materiale> listaMateiale, List<int> cantitate)
        {
            this.nrFactura = nrFacturi++;
            this.dataFactura = dataFactura;
            this.client = client;
            this.listaMateiale = listaMateiale;
            this.cantitate = cantitate;
            for (int i = 0; i < listaMateiale.Count; i++)
            {
                this.valoare += listaMateiale[i].pret * cantitate[i];
            }
        }
        public int CompareTo(object obj)
        {
            Facturi f = (Facturi)obj;
            if (this.valoare > f.valoare) { return 1; }
            else if (this.valoare < f.valoare) { return -1; }
            else return DateTime.Compare(this.dataFactura, f.dataFactura);
        }
    }
}
