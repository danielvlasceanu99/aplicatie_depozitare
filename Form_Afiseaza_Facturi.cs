﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Afiseaza_Facturi : Form
    {
        List<Facturi> auxListFacturi;
        bool ordered = false;

        public Form_Afiseaza_Facturi(List<Facturi> pListFacturi)
        {
            InitializeComponent();
            auxListFacturi = pListFacturi;
            AddLVI(auxListFacturi);
            detaliiMenuStrip.Items[0].Enabled = false;
        }
        public void AddLVI(List<Facturi> auxListFacturi)
        {
            foreach (Facturi factura in auxListFacturi)
            {
                ListViewItem item = new ListViewItem(factura.nrFactura.ToString());
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, factura.client));
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, factura.dataFactura.ToString()));
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, factura.valoare.ToString()));
                item.Tag = factura;
                lvFacturi.Items.Add(item);
            }
        }
        private void LvFacturi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvFacturi.SelectedItems.Count > 0)
            {
                detaliiMenuStrip.Items[0].Enabled = true;
            }
            else
            {
                detaliiMenuStrip.Items[0].Enabled = false;
            }
        }
        private void DetaliiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = lvFacturi.FocusedItem.Index;
            Form_Detalii_Factura detaliiFactura = new Form_Detalii_Factura(auxListFacturi[Convert.ToInt32(index)]);
            detaliiFactura.ShowDialog();
        }
        private void LvFacturi_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ordered == false)
            {
                List<Facturi> orderListFacturi = new List<Facturi>();
                foreach (Facturi f in auxListFacturi)
                {
                    orderListFacturi.Add(f);
                }
                orderListFacturi.Sort((x, y) => x.CompareTo(y));
                lvFacturi.Items.Clear();
                AddLVI(orderListFacturi);
                ordered = true;
            }
            else
            {
                lvFacturi.Items.Clear();
                AddLVI(auxListFacturi);
                ordered = false;
            }
        }
    }
}