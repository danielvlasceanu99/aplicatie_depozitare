﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.furnizoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materialeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vizualizareStocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaugaMaterialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcatiInFisierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vizualizareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emiteFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contracteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afiseazaContracteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inregistrareContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.furnizoriToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contracteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblHello = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.furnizoriToolStripMenuItem,
            this.materialeToolStripMenuItem,
            this.facturiToolStripMenuItem,
            this.contracteToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(457, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // furnizoriToolStripMenuItem
            // 
            this.furnizoriToolStripMenuItem.Name = "furnizoriToolStripMenuItem";
            this.furnizoriToolStripMenuItem.Size = new System.Drawing.Size(80, 24);
            this.furnizoriToolStripMenuItem.Text = "Furnizori";
            this.furnizoriToolStripMenuItem.Click += new System.EventHandler(this.FurnizoriToolStripMenuItem_Click);
            // 
            // materialeToolStripMenuItem
            // 
            this.materialeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vizualizareStocToolStripMenuItem,
            this.adaugaMaterialToolStripMenuItem,
            this.incarcatiInFisierToolStripMenuItem});
            this.materialeToolStripMenuItem.Name = "materialeToolStripMenuItem";
            this.materialeToolStripMenuItem.Size = new System.Drawing.Size(52, 24);
            this.materialeToolStripMenuItem.Text = "Stoc";
            // 
            // vizualizareStocToolStripMenuItem
            // 
            this.vizualizareStocToolStripMenuItem.Name = "vizualizareStocToolStripMenuItem";
            this.vizualizareStocToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.vizualizareStocToolStripMenuItem.Text = "Vizualizare stoc";
            this.vizualizareStocToolStripMenuItem.Click += new System.EventHandler(this.VizualizareStocToolStripMenuItem_Click);
            // 
            // adaugaMaterialToolStripMenuItem
            // 
            this.adaugaMaterialToolStripMenuItem.Name = "adaugaMaterialToolStripMenuItem";
            this.adaugaMaterialToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.adaugaMaterialToolStripMenuItem.Text = "Adauga material";
            this.adaugaMaterialToolStripMenuItem.Click += new System.EventHandler(this.AdaugaMaterialToolStripMenuItem_Click);
            // 
            // incarcatiInFisierToolStripMenuItem
            // 
            this.incarcatiInFisierToolStripMenuItem.Name = "incarcatiInFisierToolStripMenuItem";
            this.incarcatiInFisierToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.incarcatiInFisierToolStripMenuItem.Text = "Exporta in fisier";
            this.incarcatiInFisierToolStripMenuItem.Click += new System.EventHandler(this.IncarcatiInFisierToolStripMenuItem_Click);
            // 
            // facturiToolStripMenuItem
            // 
            this.facturiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vizualizareToolStripMenuItem,
            this.emiteFacturaToolStripMenuItem});
            this.facturiToolStripMenuItem.Name = "facturiToolStripMenuItem";
            this.facturiToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.facturiToolStripMenuItem.Text = "Facturi";
            // 
            // vizualizareToolStripMenuItem
            // 
            this.vizualizareToolStripMenuItem.Name = "vizualizareToolStripMenuItem";
            this.vizualizareToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.vizualizareToolStripMenuItem.Text = "Vizualizare facturi";
            this.vizualizareToolStripMenuItem.Click += new System.EventHandler(this.VizualizareToolStripMenuItem_Click);
            // 
            // emiteFacturaToolStripMenuItem
            // 
            this.emiteFacturaToolStripMenuItem.Name = "emiteFacturaToolStripMenuItem";
            this.emiteFacturaToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.emiteFacturaToolStripMenuItem.Text = "Emite factura";
            this.emiteFacturaToolStripMenuItem.Click += new System.EventHandler(this.EmiteFacturaToolStripMenuItem_Click);
            // 
            // contracteToolStripMenuItem
            // 
            this.contracteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afiseazaContracteToolStripMenuItem,
            this.inregistrareContractToolStripMenuItem});
            this.contracteToolStripMenuItem.Name = "contracteToolStripMenuItem";
            this.contracteToolStripMenuItem.Size = new System.Drawing.Size(87, 24);
            this.contracteToolStripMenuItem.Text = "Contracte";
            // 
            // afiseazaContracteToolStripMenuItem
            // 
            this.afiseazaContracteToolStripMenuItem.Name = "afiseazaContracteToolStripMenuItem";
            this.afiseazaContracteToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.afiseazaContracteToolStripMenuItem.Text = "Afiseaza contracte";
            this.afiseazaContracteToolStripMenuItem.Click += new System.EventHandler(this.AfiseazaContracteToolStripMenuItem_Click);
            // 
            // inregistrareContractToolStripMenuItem
            // 
            this.inregistrareContractToolStripMenuItem.Name = "inregistrareContractToolStripMenuItem";
            this.inregistrareContractToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.inregistrareContractToolStripMenuItem.Text = "Inregistrare contract";
            this.inregistrareContractToolStripMenuItem.Click += new System.EventHandler(this.InregistrareContractToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.furnizoriToolStripMenuItem1,
            this.stocToolStripMenuItem,
            this.facturiToolStripMenuItem1,
            this.contracteToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // furnizoriToolStripMenuItem1
            // 
            this.furnizoriToolStripMenuItem1.Name = "furnizoriToolStripMenuItem1";
            this.furnizoriToolStripMenuItem1.Size = new System.Drawing.Size(156, 26);
            this.furnizoriToolStripMenuItem1.Text = "Furnizori";
            this.furnizoriToolStripMenuItem1.Click += new System.EventHandler(this.FurnizoriToolStripMenuItem1_Click);
            // 
            // stocToolStripMenuItem
            // 
            this.stocToolStripMenuItem.Name = "stocToolStripMenuItem";
            this.stocToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.stocToolStripMenuItem.Text = "Stoc";
            this.stocToolStripMenuItem.Click += new System.EventHandler(this.StocToolStripMenuItem_Click);
            // 
            // facturiToolStripMenuItem1
            // 
            this.facturiToolStripMenuItem1.Name = "facturiToolStripMenuItem1";
            this.facturiToolStripMenuItem1.Size = new System.Drawing.Size(156, 26);
            this.facturiToolStripMenuItem1.Text = "Facturi";
            this.facturiToolStripMenuItem1.Click += new System.EventHandler(this.FacturiToolStripMenuItem1_Click);
            // 
            // contracteToolStripMenuItem1
            // 
            this.contracteToolStripMenuItem1.Name = "contracteToolStripMenuItem1";
            this.contracteToolStripMenuItem1.Size = new System.Drawing.Size(156, 26);
            this.contracteToolStripMenuItem1.Text = "Contracte";
            this.contracteToolStripMenuItem1.Click += new System.EventHandler(this.ContracteToolStripMenuItem1_Click);
            // 
            // lblHello
            // 
            this.lblHello.AutoSize = true;
            this.lblHello.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblHello.Location = new System.Drawing.Point(8, 80);
            this.lblHello.Name = "lblHello";
            this.lblHello.Size = new System.Drawing.Size(0, 31);
            this.lblHello.TabIndex = 1;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 470);
            this.Controls.Add(this.lblHello);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form_Main";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem furnizoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materialeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vizualizareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emiteFacturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contracteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inregistrareContractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afiseazaContracteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vizualizareStocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaugaMaterialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcatiInFisierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem furnizoriToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contracteToolStripMenuItem1;
        private System.Windows.Forms.Label lblHello;
    }
}

