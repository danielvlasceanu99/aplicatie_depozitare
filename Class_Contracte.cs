﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project___Activitatea_de_aprovizionare
{
    public class Contracte
    {
        public int nrContract;
        public Furnizori furnizor;
        public DateTime dataContract;
        public static int numarrContracte = 1;

        public Contracte() { nrContract = numarrContracte++; }
        public Contracte(Furnizori furnizor, DateTime dataContract)
        {
            nrContract = numarrContracte++;
            this.dataContract = dataContract;
            this.furnizor = furnizor;
        }

    }
}
