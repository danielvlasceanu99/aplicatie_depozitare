﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Afiseaza_Facturi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lvFacturi = new System.Windows.Forms.ListView();
            this.Numar = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Client = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Valoare = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.detaliiMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.detaliiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detaliiMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(175, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Lista facturilor";
            // 
            // lvFacturi
            // 
            this.lvFacturi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Numar,
            this.Client,
            this.Data,
            this.Valoare});
            this.lvFacturi.ContextMenuStrip = this.detaliiMenuStrip;
            this.lvFacturi.HideSelection = false;
            this.lvFacturi.Location = new System.Drawing.Point(12, 51);
            this.lvFacturi.Name = "lvFacturi";
            this.lvFacturi.Size = new System.Drawing.Size(400, 571);
            this.lvFacturi.TabIndex = 1;
            this.lvFacturi.UseCompatibleStateImageBehavior = false;
            this.lvFacturi.View = System.Windows.Forms.View.Details;
            this.lvFacturi.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvFacturi_ColumnClick);
            this.lvFacturi.SelectedIndexChanged += new System.EventHandler(this.LvFacturi_SelectedIndexChanged);
            // 
            // Numar
            // 
            this.Numar.Text = "#";
            this.Numar.Width = 30;
            // 
            // Client
            // 
            this.Client.Text = "Client";
            // 
            // Data
            // 
            this.Data.Text = "Data";
            this.Data.Width = 140;
            // 
            // Valoare
            // 
            this.Valoare.Text = "Valoare";
            this.Valoare.Width = 65;
            // 
            // detaliiMenuStrip
            // 
            this.detaliiMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.detaliiMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detaliiToolStripMenuItem});
            this.detaliiMenuStrip.Name = "detaliiMenuStrip";
            this.detaliiMenuStrip.Size = new System.Drawing.Size(123, 28);
            this.detaliiMenuStrip.Text = "Detalii";
            // 
            // detaliiToolStripMenuItem
            // 
            this.detaliiToolStripMenuItem.Name = "detaliiToolStripMenuItem";
            this.detaliiToolStripMenuItem.Size = new System.Drawing.Size(122, 24);
            this.detaliiToolStripMenuItem.Text = "Detalii";
            this.detaliiToolStripMenuItem.Click += new System.EventHandler(this.DetaliiToolStripMenuItem_Click);
            // 
            // Form_Afiseaza_Facturi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(422, 634);
            this.Controls.Add(this.lvFacturi);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Afiseaza_Facturi";
            this.Text = "Form_Afiseaza_Facturi";
            this.detaliiMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ListView lvFacturi;
        private System.Windows.Forms.ColumnHeader Numar;
        private System.Windows.Forms.ColumnHeader Data;
        private System.Windows.Forms.ColumnHeader Client;
        private System.Windows.Forms.ColumnHeader Valoare;
        private System.Windows.Forms.ContextMenuStrip detaliiMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem detaliiToolStripMenuItem;
    }
}