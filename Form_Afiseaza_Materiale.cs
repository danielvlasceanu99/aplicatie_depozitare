﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Afiseaza_Materiale : Form
    {
        List<TextBox> boxes = new List<TextBox>();
        List<Materiale> auxMateriale = new List<Materiale>();

        public Form_Afiseaza_Materiale(List<Materiale> pMateriale)
        {
            InitializeComponent();
            txtID.Text = pMateriale[0].idMaterial.ToString();
            txtDenumire.Text = pMateriale[0].denumireMaterial;
            txtFurnizor.Text = pMateriale[0].furnizor.ToString();
            txtStoc.Text = pMateriale[0].stocCurent.ToString();
            txtPret.Text = pMateriale[0].pret.ToString();

            auxMateriale = pMateriale;

            for(int i = 1; i<pMateriale.Count; i++)
            {
                TextBox id = new TextBox();
                id.Enabled = false;
                id.Location = new Point(txtID.Location.X, txtID.Location.Y + i * 25);
                id.Name = "txtID" + (i + 1).ToString();
                id.Size = txtID.Size;
                id.TabIndex = txtID.TabIndex * i + 1;
                id.Text = pMateriale[i].idMaterial.ToString();

                TextBox denumire = new TextBox();
                denumire.Enabled = false;
                denumire.Location = new Point(txtDenumire.Location.X, txtDenumire.Location.Y + i * 25);
                denumire.Name = "txtDenumire" + (i + 1).ToString();
                denumire.Size = txtDenumire.Size;
                denumire.TabIndex = txtDenumire.TabIndex * i + 1;
                denumire.Text = pMateriale[i].denumireMaterial;

                TextBox furnizor = new TextBox();
                furnizor.Enabled = false;
                furnizor.Location = new Point(txtFurnizor.Location.X, txtFurnizor.Location.Y + i * 25);
                furnizor.Name = "txtFurnizor" + (i + 1).ToString();
                furnizor.Size = txtFurnizor.Size;
                furnizor.TabIndex = txtFurnizor.TabIndex * i + 1;
                furnizor.Text = pMateriale[i].furnizor;

                TextBoxInt stoc = new TextBoxInt();
                stoc.Enabled = false;
                stoc.Location = new Point(txtStoc.Location.X, txtStoc.Location.Y + i * 25);
                stoc.Name = "txtStoc" + (i + 1).ToString();
                stoc.Size = txtStoc.Size;
                stoc.TabIndex = txtStoc.TabIndex * i + 1;
                stoc.Text = pMateriale[i].stocCurent.ToString();

                TextBoxFloat pret = new TextBoxFloat();
                pret.Enabled = false;
                pret.Location = new Point(txtPret.Location.X, txtPret.Location.Y + i * 25);
                pret.Name = "txtPret" + (i + 1).ToString();
                pret.Size = txtPret.Size;
                pret.TabIndex = txtPret.TabIndex * i + 1;
                pret.Text = pMateriale[i].pret.ToString();

                Controls.Add(id); boxes.Add(id);
                Controls.Add(denumire); boxes.Add(denumire);
                Controls.Add(furnizor); boxes.Add(furnizor);
                Controls.Add(stoc); boxes.Add(stoc);
                Controls.Add(pret); boxes.Add(pret);
            }
        }
        private void BtnModifica_Click(object sender, EventArgs e)
        {
            txtPret.Enabled = true;
            txtStoc.Enabled = true;
            btnSave.Enabled = true;
            for(int i = 0; i < auxMateriale.Count - 1; i++)
            {
                boxes[i * 5 + 3].Enabled = true;
                boxes[i * 5 + 4].Enabled = true;
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            bool error = false;
            if(txtStoc.Text != "")
            {
                auxMateriale[0].stocCurent = Convert.ToInt32(txtStoc.Text);
                txtStoc.Enabled = false;
            }
            else
            {
                MessageBox.Show("Stocul materialului 1 nu poate fi gol.");
                error = true;
            }

            if (txtPret.Text != "" && Convert.ToDecimal(txtPret.Text) != 0)
            {
                auxMateriale[0].pret = Convert.ToDouble(txtPret.Text);
                txtPret.Enabled = false;
            }
            else
            {
                MessageBox.Show("Preturl materialului 1 este invalid.");
                error = true;
            }

            for (int i = 0; i < auxMateriale.Count - 1; i++)
            {
                if (boxes[i * 5 + 3].Text != "")
                {
                    auxMateriale[i + 1].stocCurent = Convert.ToInt32(boxes[i * 5 + 3].Text);
                    boxes[i * 5 + 3].Enabled = false;
                }
                else
                {
                    MessageBox.Show("Stocul materialului " + (i + 2).ToString() + " nu poate fi gol.");
                    error = true;
                }

                if (boxes[i * 5 + 4].Text != "" && Convert.ToDecimal(boxes[i * 5 + 4].Text) != 0)
                {
                    auxMateriale[i + 1].pret = Convert.ToDouble(boxes[i * 5 + 4].Text);
                    boxes[i * 5 + 4].Enabled = false;
                }
                else
                {
                    MessageBox.Show("Pretul materialului " + (i + 2).ToString() + " este invalid");
                    error = true;
                }
                if(error == true) { BtnModifica_Click(sender, e); }
            }
        }
    }
}