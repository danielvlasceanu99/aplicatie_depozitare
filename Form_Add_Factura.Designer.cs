﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Add_Factura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblMateriale = new System.Windows.Forms.Label();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.lblCantitate = new System.Windows.Forms.Label();
            this.cbMaterial = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtCantitate = new Project___Activitatea_de_aprovizionare.TextBoxFloat();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnSubtotal = new System.Windows.Forms.Button();
            this.ttDelete = new System.Windows.Forms.ToolTip(this.components);
            this.ttAdd = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Underline);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(201, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Factura numarul ";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(72, 57);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(374, 22);
            this.txtClient.TabIndex = 1;
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblClient.Location = new System.Drawing.Point(8, 56);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(58, 22);
            this.lblClient.TabIndex = 2;
            this.lblClient.Text = "Client";
            // 
            // lblMateriale
            // 
            this.lblMateriale.AutoSize = true;
            this.lblMateriale.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.lblMateriale.Location = new System.Drawing.Point(12, 133);
            this.lblMateriale.Name = "lblMateriale";
            this.lblMateriale.Size = new System.Drawing.Size(196, 25);
            this.lblMateriale.TabIndex = 3;
            this.lblMateriale.Text = "Materiale comandate";
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblMaterial.Location = new System.Drawing.Point(55, 182);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(78, 22);
            this.lblMaterial.TabIndex = 4;
            this.lblMaterial.Text = "Material";
            // 
            // lblCantitate
            // 
            this.lblCantitate.AutoSize = true;
            this.lblCantitate.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblCantitate.Location = new System.Drawing.Point(298, 182);
            this.lblCantitate.Name = "lblCantitate";
            this.lblCantitate.Size = new System.Drawing.Size(80, 22);
            this.lblCantitate.TabIndex = 5;
            this.lblCantitate.Text = "Cantitate";
            // 
            // cbMaterial
            // 
            this.cbMaterial.FormattingEnabled = true;
            this.cbMaterial.Location = new System.Drawing.Point(55, 207);
            this.cbMaterial.Name = "cbMaterial";
            this.cbMaterial.Size = new System.Drawing.Size(241, 24);
            this.cbMaterial.TabIndex = 6;
            this.cbMaterial.SelectedIndexChanged += new System.EventHandler(this.CbMaterial_SelectedIndexChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(12, 207);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(37, 24);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "🗑";
            this.ttDelete.SetToolTip(this.btnDelete, "Sterge linia");
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Enabled = false;
            this.btnAdd.Location = new System.Drawing.Point(414, 207);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(37, 24);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "+";
            this.ttAdd.SetToolTip(this.btnAdd, "Adauga o linie");
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnSave.Location = new System.Drawing.Point(359, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 31);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "💾 Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtCantitate
            // 
            this.txtCantitate.Enabled = false;
            this.txtCantitate.Location = new System.Drawing.Point(302, 207);
            this.txtCantitate.Name = "txtCantitate";
            this.txtCantitate.Size = new System.Drawing.Size(106, 22);
            this.txtCantitate.TabIndex = 11;
            this.txtCantitate.TextChanged += new System.EventHandler(this.TxtCantitate_TextChanged);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTotal.Location = new System.Drawing.Point(355, 93);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(72, 22);
            this.lblTotal.TabIndex = 12;
            this.lblTotal.Text = "Total: 0";
            // 
            // btnSubtotal
            // 
            this.btnSubtotal.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtotal.Location = new System.Drawing.Point(12, 93);
            this.btnSubtotal.Name = "btnSubtotal";
            this.btnSubtotal.Size = new System.Drawing.Size(105, 29);
            this.btnSubtotal.TabIndex = 13;
            this.btnSubtotal.Text = "Subtotal";
            this.btnSubtotal.UseVisualStyleBackColor = true;
            this.btnSubtotal.Click += new System.EventHandler(this.BtnSubtotal_Click);
            // 
            // ttDelete
            // 
            this.ttDelete.AutoPopDelay = 5000;
            this.ttDelete.InitialDelay = 10;
            this.ttDelete.ReshowDelay = 100;
            // 
            // ttAdd
            // 
            this.ttAdd.AutoPopDelay = 5000;
            this.ttAdd.InitialDelay = 10;
            this.ttAdd.ReshowDelay = 100;
            // 
            // Form_Add_Factura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(458, 634);
            this.Controls.Add(this.btnSubtotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.txtCantitate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.cbMaterial);
            this.Controls.Add(this.lblCantitate);
            this.Controls.Add(this.lblMaterial);
            this.Controls.Add(this.lblMateriale);
            this.Controls.Add(this.lblClient);
            this.Controls.Add(this.txtClient);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Add_Factura";
            this.Text = "Form_Add_Factura";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblMateriale;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.Label lblCantitate;
        private System.Windows.Forms.ComboBox cbMaterial;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private TextBoxFloat txtCantitate;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnSubtotal;
        private System.Windows.Forms.ToolTip ttDelete;
        private System.Windows.Forms.ToolTip ttAdd;
    }
}