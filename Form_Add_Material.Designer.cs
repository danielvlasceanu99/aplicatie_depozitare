﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Add_Material
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblDenumire = new System.Windows.Forms.Label();
            this.lblPret = new System.Windows.Forms.Label();
            this.lblFurnizor = new System.Windows.Forms.Label();
            this.txtDenumire = new System.Windows.Forms.TextBox();
            this.cbFurnizor = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPret = new Project___Activitatea_de_aprovizionare.TextBoxFloat();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(113, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Material ";
            // 
            // lblDenumire
            // 
            this.lblDenumire.AutoSize = true;
            this.lblDenumire.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblDenumire.Location = new System.Drawing.Point(12, 54);
            this.lblDenumire.Name = "lblDenumire";
            this.lblDenumire.Size = new System.Drawing.Size(87, 22);
            this.lblDenumire.TabIndex = 1;
            this.lblDenumire.Text = "Denumire";
            // 
            // lblPret
            // 
            this.lblPret.AutoSize = true;
            this.lblPret.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblPret.Location = new System.Drawing.Point(12, 84);
            this.lblPret.Name = "lblPret";
            this.lblPret.Size = new System.Drawing.Size(42, 22);
            this.lblPret.TabIndex = 2;
            this.lblPret.Text = "Pret";
            // 
            // lblFurnizor
            // 
            this.lblFurnizor.AutoSize = true;
            this.lblFurnizor.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblFurnizor.Location = new System.Drawing.Point(12, 114);
            this.lblFurnizor.Name = "lblFurnizor";
            this.lblFurnizor.Size = new System.Drawing.Size(77, 22);
            this.lblFurnizor.TabIndex = 3;
            this.lblFurnizor.Text = "Furnizor";
            // 
            // txtDenumire
            // 
            this.txtDenumire.Location = new System.Drawing.Point(109, 55);
            this.txtDenumire.Name = "txtDenumire";
            this.txtDenumire.Size = new System.Drawing.Size(244, 22);
            this.txtDenumire.TabIndex = 4;
            // 
            // txtPret
            // 
            this.txtPret.Location = new System.Drawing.Point(109, 85);
            this.txtPret.Name = "txtPret";
            this.txtPret.Size = new System.Drawing.Size(244, 22);
            this.txtPret.TabIndex = 5;
            // 
            // cbFurnizor
            // 
            this.cbFurnizor.FormattingEnabled = true;
            this.cbFurnizor.Location = new System.Drawing.Point(109, 118);
            this.cbFurnizor.Name = "cbFurnizor";
            this.cbFurnizor.Size = new System.Drawing.Size(244, 24);
            this.cbFurnizor.TabIndex = 6;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.btnSave.Location = new System.Drawing.Point(258, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(95, 39);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "💾 Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Form_Add_Material
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(365, 201);
            this.Controls.Add(this.txtPret);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cbFurnizor);
            this.Controls.Add(this.txtDenumire);
            this.Controls.Add(this.lblFurnizor);
            this.Controls.Add(this.lblPret);
            this.Controls.Add(this.lblDenumire);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Add_Material";
            this.Text = "addmaterialForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblDenumire;
        private System.Windows.Forms.Label lblPret;
        private System.Windows.Forms.Label lblFurnizor;
        private System.Windows.Forms.TextBox txtDenumire;
        private System.Windows.Forms.ComboBox cbFurnizor;
        private System.Windows.Forms.Button btnSave;
        private TextBoxFloat txtPret;
    }
}