﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Afiseaza_Materiale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblDenumire = new System.Windows.Forms.Label();
            this.lblFurnizor = new System.Windows.Forms.Label();
            this.lblStoc = new System.Windows.Forms.Label();
            this.lblPret = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtDenumire = new System.Windows.Forms.TextBox();
            this.txtFurnizor = new System.Windows.Forms.TextBox();
            this.btnModifica = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPret = new Project___Activitatea_de_aprovizionare.TextBoxFloat();
            this.txtStoc = new Project___Activitatea_de_aprovizionare.TextBoxInt();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(118, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Materiale";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblID.Location = new System.Drawing.Point(14, 55);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(30, 22);
            this.lblID.TabIndex = 1;
            this.lblID.Text = "ID";
            // 
            // lblDenumire
            // 
            this.lblDenumire.AutoSize = true;
            this.lblDenumire.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblDenumire.Location = new System.Drawing.Point(50, 55);
            this.lblDenumire.Name = "lblDenumire";
            this.lblDenumire.Size = new System.Drawing.Size(87, 22);
            this.lblDenumire.TabIndex = 2;
            this.lblDenumire.Text = "Denumire";
            // 
            // lblFurnizor
            // 
            this.lblFurnizor.AutoSize = true;
            this.lblFurnizor.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblFurnizor.Location = new System.Drawing.Point(188, 55);
            this.lblFurnizor.Name = "lblFurnizor";
            this.lblFurnizor.Size = new System.Drawing.Size(77, 22);
            this.lblFurnizor.TabIndex = 3;
            this.lblFurnizor.Text = "Furnizor";
            // 
            // lblStoc
            // 
            this.lblStoc.AutoSize = true;
            this.lblStoc.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblStoc.Location = new System.Drawing.Point(327, 55);
            this.lblStoc.Name = "lblStoc";
            this.lblStoc.Size = new System.Drawing.Size(45, 22);
            this.lblStoc.TabIndex = 4;
            this.lblStoc.Text = "Stoc";
            // 
            // lblPret
            // 
            this.lblPret.AutoSize = true;
            this.lblPret.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblPret.Location = new System.Drawing.Point(378, 55);
            this.lblPret.Name = "lblPret";
            this.lblPret.Size = new System.Drawing.Size(42, 22);
            this.lblPret.TabIndex = 5;
            this.lblPret.Text = "Pret";
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(12, 80);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(32, 22);
            this.txtID.TabIndex = 6;
            // 
            // txtDenumire
            // 
            this.txtDenumire.Enabled = false;
            this.txtDenumire.Location = new System.Drawing.Point(50, 80);
            this.txtDenumire.Name = "txtDenumire";
            this.txtDenumire.Size = new System.Drawing.Size(133, 22);
            this.txtDenumire.TabIndex = 7;
            // 
            // txtFurnizor
            // 
            this.txtFurnizor.Enabled = false;
            this.txtFurnizor.Location = new System.Drawing.Point(189, 80);
            this.txtFurnizor.Name = "txtFurnizor";
            this.txtFurnizor.Size = new System.Drawing.Size(133, 22);
            this.txtFurnizor.TabIndex = 8;
            // 
            // btnModifica
            // 
            this.btnModifica.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnModifica.Location = new System.Drawing.Point(257, 9);
            this.btnModifica.Name = "btnModifica";
            this.btnModifica.Size = new System.Drawing.Size(92, 31);
            this.btnModifica.TabIndex = 11;
            this.btnModifica.Text = "Modifica";
            this.btnModifica.UseVisualStyleBackColor = true;
            this.btnModifica.Click += new System.EventHandler(this.BtnModifica_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.btnSave.Location = new System.Drawing.Point(355, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 31);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "💾 Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtPret
            // 
            this.txtPret.Enabled = false;
            this.txtPret.Location = new System.Drawing.Point(382, 80);
            this.txtPret.Name = "txtPret";
            this.txtPret.Size = new System.Drawing.Size(69, 22);
            this.txtPret.TabIndex = 13;
            // 
            // txtStoc
            // 
            this.txtStoc.Enabled = false;
            this.txtStoc.Location = new System.Drawing.Point(328, 80);
            this.txtStoc.Name = "txtStoc";
            this.txtStoc.Size = new System.Drawing.Size(48, 22);
            this.txtStoc.TabIndex = 14;
            // 
            // Form_Afiseaza_Materiale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(463, 198);
            this.Controls.Add(this.txtStoc);
            this.Controls.Add(this.txtPret);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnModifica);
            this.Controls.Add(this.txtFurnizor);
            this.Controls.Add(this.txtDenumire);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.lblPret);
            this.Controls.Add(this.lblStoc);
            this.Controls.Add(this.lblFurnizor);
            this.Controls.Add(this.lblDenumire);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Afiseaza_Materiale";
            this.Text = "afiseazaMaterialeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblDenumire;
        private System.Windows.Forms.Label lblFurnizor;
        private System.Windows.Forms.Label lblStoc;
        private System.Windows.Forms.Label lblPret;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtDenumire;
        private System.Windows.Forms.TextBox txtFurnizor;
        private System.Windows.Forms.Button btnModifica;
        private System.Windows.Forms.Button btnSave;
        private TextBoxFloat txtPret;
        private TextBoxInt txtStoc;
    }
}