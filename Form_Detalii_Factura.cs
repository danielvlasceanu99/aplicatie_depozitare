﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Detalii_Factura : Form
    {
        public Form_Detalii_Factura(Facturi pFactura)
        {
            InitializeComponent();
            lblTitle.Text += pFactura.nrFactura;
            lblData.Text = pFactura.dataFactura.Day.ToString();
            lblData.Text += "." + pFactura.dataFactura.Month.ToString();
            lblData.Text += "." + pFactura.dataFactura.Year.ToString();
            txtMaterial1.Text = pFactura.listaMateiale[0].denumireMaterial;
            txtPret1.Text = pFactura.listaMateiale[0].pret.ToString();
            txtCantitate1.Text = " * " + pFactura.cantitate[0].ToString();
            txtSubtotal1.Text = (pFactura.listaMateiale[0].pret * pFactura.cantitate[0]).ToString();
            lblClient.Text = pFactura.client;
            lblValoare.Text = pFactura.valoare.ToString();

            for(int i = 1; i<pFactura.listaMateiale.Count; i++)
            {
                TextBox txtMaterial = new TextBox();
                txtMaterial.Enabled = false;
                txtMaterial.Font = txtMaterial1.Font;
                txtMaterial.Location = new Point(txtMaterial1.Location.X, txtMaterial1.Location.Y + 50 * i);
                txtMaterial.Name = "lblmaterial" + (i + 1).ToString();
                txtMaterial.Size = txtMaterial1.Size;
                txtMaterial.Text = pFactura.listaMateiale[i].denumireMaterial;
                Controls.Add(txtMaterial);

                TextBox txtPret = new TextBox();
                txtPret.Enabled = false;
                txtPret.Font = txtPret1.Font;
                txtPret.Location = new Point(txtPret1.Location.X, txtPret1.Location.Y + 50 * i);
                txtPret.Name = "lblPret" + (i + 1).ToString();
                txtPret.Size = txtPret1.Size;
                txtPret.Text = pFactura.listaMateiale[i].pret.ToString();
                Controls.Add(txtPret);

                TextBox txtCantitate = new TextBox();
                txtCantitate.Enabled = false;
                txtCantitate.Font = txtCantitate1.Font;
                txtCantitate.Location = new Point(txtCantitate1.Location.X, txtCantitate1.Location.Y + 50 * i);
                txtCantitate.Name = "lblCantitate" + (i + 1).ToString();
                txtCantitate.Size = txtCantitate1.Size;
                txtCantitate.Text = " * " + pFactura.cantitate[i].ToString();
                Controls.Add(txtCantitate);

                TextBox txtSubTotal = new TextBox();
                txtSubTotal.Enabled = false;
                txtSubTotal.Font = txtSubtotal1.Font;
                txtSubTotal.Location = new Point(txtSubtotal1.Location.X, txtSubtotal1.Location.Y + 50 * i);
                txtSubTotal.Name = "lblTotal" + (i + 1).ToString();
                txtSubTotal.Size = txtSubtotal1.Size;
                txtSubTotal.Text = (pFactura.listaMateiale[i].pret * pFactura.cantitate[i]).ToString();
                Controls.Add(txtSubTotal);

                lblTotalText.Location = new Point(lblTotalText.Location.X, lblTotalText.Location.Y + 50);
                lblValoare.Location = new Point(lblValoare.Location.X, lblValoare.Location.Y + 50);
            }
        }
    }
}