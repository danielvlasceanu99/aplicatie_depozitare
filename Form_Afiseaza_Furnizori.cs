﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Afiseaza_Furnizori : Form
    {
        int nrLinii = 0;
        int distanta = 25;
        List<Furnizori> auxListFurnizori;
        List<linie> newLine = new List<linie>();

        public Form_Afiseaza_Furnizori(List<Furnizori> pListFurnizori)
        {
            InitializeComponent();
            auxListFurnizori = pListFurnizori;

            // Verify if the list is empty;
            if (auxListFurnizori.Count == 0)
            {
                // If it is, we will show a message that says that;
                Label lblNothing = new Label();
                lblNothing.AutoSize = true;
                lblNothing.Font = new System.Drawing.Font("Times New Roman", 16F);
                lblNothing.Location = new Point(lblTitle.Location.X, 50);
                lblNothing.Name = "lblNothing";
                lblNothing.Size = lblTitle.Size;
                lblNothing.TabIndex = 1;
                lblNothing.Text = "Nu exista furnizori !";
                Controls.Add(lblNothing);
            }
            else
            {
                // If there are items in the list, we will create aline for everyone;
                
                // Create the table header
                // idFurnizor:
                Label lblID = new Label();
                lblID.AutoSize = true;
                lblID.Font = new System.Drawing.Font("Times New Roman", 13F);
                lblID.Location = new Point(26, 72);
                lblID.Name = "lblID";
                lblID.Size = new Size(12, 10);
                lblID.TabIndex = 900;
                lblID.Text = "#";
                Controls.Add(lblID);

                // Furnizor
                Label lblFurnizor = new Label();
                lblFurnizor.AutoSize = true;
                lblFurnizor.Font = new System.Drawing.Font("Times New Roman", 13F);
                lblFurnizor.Location = new Point(156, 72);
                lblFurnizor.Name = "lblID";
                lblFurnizor.Size = new Size(50, 10);
                lblFurnizor.TabIndex = 901;
                lblFurnizor.Text = "Furnizor";
                Controls.Add(lblFurnizor);

                // For each item in the list we will create a new line
                foreach (Furnizori aux in auxListFurnizori) {
                    // Create and initialize ID textbox:
                    TextBox txtID = new TextBox();
                    txtID.Enabled = false;
                    txtID.Location = new Point(18, 96 + nrLinii * distanta);
                    txtID.Name = "txtID" + (nrLinii + 1).ToString();
                    txtID.Size = new Size(30, 22);
                    txtID.TabIndex = 3 * nrLinii + 1;
                    txtID.Text = aux.idFurnizor.ToString();
                    
                    // Create and initialize ID textbox: 
                    TextBox txtFurnizor = new TextBox();
                    txtFurnizor.Enabled = false;
                    txtFurnizor.Location = new Point(57, 96 + nrLinii * distanta);
                    txtFurnizor.Name = "txtFurnizor" + (nrLinii + 1).ToString();
                    txtFurnizor.Size = new Size(234, 22);
                    txtFurnizor.TabIndex = 3 * nrLinii + 2;
                    txtFurnizor.Text = aux.numeFurnizor;

                    // Create and initialize the details button:
                    Button btnDetalii = new Button();
                    btnDetalii.Enabled = true;
                    btnDetalii.Name = "btnDetalii" + (nrLinii + 1).ToString();
                    btnDetalii.Location = new Point(300, 95 + nrLinii * distanta);
                    btnDetalii.Size = new Size(75, 23);
                    btnDetalii.TabIndex = 3 * nrLinii + 3;
                    btnDetalii.Text = "Detalii";
                    btnDetalii.Click += new EventHandler(BtnDetalii_Click);

                    // Add the controlers to the form:
                    Controls.Add(txtID);
                    Controls.Add(txtFurnizor);
                    Controls.Add(btnDetalii);

                    // Create and initalize the line:
                    linie nou = new linie();
                    nou.IdTXT = txtID;
                    nou.FurnizorTXT = txtFurnizor;
                    nou.DetaliiBTN = btnDetalii;
                    newLine.Add(nou);
                    nrLinii++;
                }
            }
        }
        private void BtnDetalii_Click(object sender, EventArgs e)
        {
            linie curent = newLine.Find(m => ((linie)m).DetaliiBTN == (Button)sender);
            int index = newLine.IndexOf(curent);
            Form_Detalii_Furnizor detaliiFurnizor = new Form_Detalii_Furnizor(auxListFurnizori[index]);
            detaliiFurnizor.ShowDialog();
        }
        public class linie
        {
            public object IdTXT;
            public object FurnizorTXT;
            public object DetaliiBTN;
        }
    }
}