﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Detalii_Furnizor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblIdFurnizor = new System.Windows.Forms.Label();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnModifica = new System.Windows.Forms.Button();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblSubtitle2 = new System.Windows.Forms.Label();
            this.txtMaterial1 = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSubtitle1 = new System.Windows.Forms.Label();
            this.txtTelefon = new Project___Activitatea_de_aprovizionare.TextBoxInt();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F);
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(108, 31);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Furnizor";
            // 
            // lblIdFurnizor
            // 
            this.lblIdFurnizor.AutoSize = true;
            this.lblIdFurnizor.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblIdFurnizor.Location = new System.Drawing.Point(14, 40);
            this.lblIdFurnizor.Name = "lblIdFurnizor";
            this.lblIdFurnizor.Size = new System.Drawing.Size(41, 22);
            this.lblIdFurnizor.TabIndex = 1;
            this.lblIdFurnizor.Text = "ID: ";
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblAdresa.Location = new System.Drawing.Point(20, 150);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(56, 19);
            this.lblAdresa.TabIndex = 2;
            this.lblAdresa.Text = "Adresa";
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTelefon.Location = new System.Drawing.Point(20, 192);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(61, 19);
            this.lblTelefon.TabIndex = 3;
            this.lblTelefon.Text = "Telefon";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblEmail.Location = new System.Drawing.Point(20, 234);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(49, 19);
            this.lblEmail.TabIndex = 4;
            this.lblEmail.Text = "Email";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnExit.Location = new System.Drawing.Point(12, 411);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 32);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnModifica
            // 
            this.btnModifica.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnModifica.Location = new System.Drawing.Point(285, 96);
            this.btnModifica.Name = "btnModifica";
            this.btnModifica.Size = new System.Drawing.Size(92, 35);
            this.btnModifica.TabIndex = 6;
            this.btnModifica.Text = "Modifica";
            this.btnModifica.UseVisualStyleBackColor = true;
            this.btnModifica.Click += new System.EventHandler(this.BtnModifica_Click);
            // 
            // txtAdresa
            // 
            this.txtAdresa.Enabled = false;
            this.txtAdresa.Location = new System.Drawing.Point(95, 147);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(282, 22);
            this.txtAdresa.TabIndex = 7;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Enabled = false;
            this.txtTelefon.Location = new System.Drawing.Point(95, 192);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(282, 22);
            this.txtTelefon.TabIndex = 8;
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(95, 231);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(282, 22);
            this.txtEmail.TabIndex = 9;
            // 
            // lblSubtitle2
            // 
            this.lblSubtitle2.AutoSize = true;
            this.lblSubtitle2.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblSubtitle2.Location = new System.Drawing.Point(14, 288);
            this.lblSubtitle2.Name = "lblSubtitle2";
            this.lblSubtitle2.Size = new System.Drawing.Size(207, 22);
            this.lblSubtitle2.TabIndex = 10;
            this.lblSubtitle2.Text = "Lista peoduselor furniate";
            // 
            // txtMaterial1
            // 
            this.txtMaterial1.Enabled = false;
            this.txtMaterial1.Location = new System.Drawing.Point(12, 325);
            this.txtMaterial1.Name = "txtMaterial1";
            this.txtMaterial1.Size = new System.Drawing.Size(365, 22);
            this.txtMaterial1.TabIndex = 11;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnSave.Location = new System.Drawing.Point(285, 411);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(92, 32);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "💾 Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblSubtitle1
            // 
            this.lblSubtitle1.AutoSize = true;
            this.lblSubtitle1.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblSubtitle1.Location = new System.Drawing.Point(14, 99);
            this.lblSubtitle1.Name = "lblSubtitle1";
            this.lblSubtitle1.Size = new System.Drawing.Size(132, 22);
            this.lblSubtitle1.TabIndex = 13;
            this.lblSubtitle1.Text = "Detalii furnizor";
            // 
            // Form_Detalii_Furnizor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(389, 463);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.lblSubtitle1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtMaterial1);
            this.Controls.Add(this.lblSubtitle2);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.btnModifica);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.lblAdresa);
            this.Controls.Add(this.lblIdFurnizor);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form_Detalii_Furnizor";
            this.Text = "DetaliiFurnizorForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblIdFurnizor;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnModifica;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblSubtitle2;
        private System.Windows.Forms.TextBox txtMaterial1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSubtitle1;
        private TextBoxInt txtTelefon;
    }
}