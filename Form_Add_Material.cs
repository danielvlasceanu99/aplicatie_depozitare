﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Add_Material : Form
    {
        Materiale auxMaterial;

        public Form_Add_Material(Materiale pMaterial, List<Furnizori> pListFurnizori)
        {
            InitializeComponent();
            auxMaterial = pMaterial;
            foreach(Furnizori auxfurnizori in pListFurnizori)
            {
                cbFurnizor.Items.Add(auxfurnizori.numeFurnizor);
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            bool err = false;
            if(txtDenumire.Text != "") 
            {
                auxMaterial.denumireMaterial = txtDenumire.Text;
            }
            else { MessageBox.Show("Produsul trebuie sa aiba o denumire."); err = true; }
            if(txtPret.Text.Length != 0)
            {
                auxMaterial.pret = Convert.ToDouble(txtPret.Text);
            }
            else { MessageBox.Show("Produsul trebuie sa aiba un pret."); err = true; }
            if(cbFurnizor.SelectedIndex > -1)
            {
                auxMaterial.furnizor = cbFurnizor.SelectedItem.ToString();
            }
            else { MessageBox.Show("Alegeti un furnizor."); err = true; }

            if (err == false)
            {
                btnSave.DialogResult = DialogResult.OK;
                DialogResult = btnSave.DialogResult;
            }
        }
    }
}