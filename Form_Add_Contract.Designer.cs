﻿namespace Project___Activitatea_de_aprovizionare
{
    partial class Form_Add_Contract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.f2Titlu = new System.Windows.Forms.Label();
            this.lblFurnizor = new System.Windows.Forms.Label();
            this.txtFurnizor = new System.Windows.Forms.TextBox();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblMateriale = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnSterge1 = new System.Windows.Forms.Button();
            this.txtMaterial1 = new System.Windows.Forms.TextBox();
            this.btnNewLine = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTelefon = new Project___Activitatea_de_aprovizionare.TextBoxInt();
            this.ttDelete = new System.Windows.Forms.ToolTip(this.components);
            this.ttAdd = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // f2Titlu
            // 
            this.f2Titlu.AutoSize = true;
            this.f2Titlu.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.f2Titlu.Location = new System.Drawing.Point(12, 69);
            this.f2Titlu.Name = "f2Titlu";
            this.f2Titlu.Size = new System.Drawing.Size(223, 27);
            this.f2Titlu.TabIndex = 0;
            this.f2Titlu.Text = "Introduceti Furnizorul";
            // 
            // lblFurnizor
            // 
            this.lblFurnizor.AutoSize = true;
            this.lblFurnizor.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFurnizor.Location = new System.Drawing.Point(16, 109);
            this.lblFurnizor.Name = "lblFurnizor";
            this.lblFurnizor.Size = new System.Drawing.Size(114, 19);
            this.lblFurnizor.TabIndex = 1;
            this.lblFurnizor.Text = "Nume furnizor:";
            // 
            // txtFurnizor
            // 
            this.txtFurnizor.Location = new System.Drawing.Point(145, 109);
            this.txtFurnizor.Name = "txtFurnizor";
            this.txtFurnizor.Size = new System.Drawing.Size(197, 22);
            this.txtFurnizor.TabIndex = 2;
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblAdresa.Location = new System.Drawing.Point(16, 143);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(61, 19);
            this.lblAdresa.TabIndex = 3;
            this.lblAdresa.Text = "Ardesa:";
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefon.Location = new System.Drawing.Point(16, 177);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(66, 19);
            this.lblTelefon.TabIndex = 5;
            this.lblTelefon.Text = "Telefon:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblEmail.Location = new System.Drawing.Point(16, 212);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(54, 19);
            this.lblEmail.TabIndex = 7;
            this.lblEmail.Text = "Email:";
            // 
            // lblMateriale
            // 
            this.lblMateriale.AutoSize = true;
            this.lblMateriale.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblMateriale.Location = new System.Drawing.Point(16, 307);
            this.lblMateriale.Name = "lblMateriale";
            this.lblMateriale.Size = new System.Drawing.Size(206, 22);
            this.lblMateriale.TabIndex = 9;
            this.lblMateriale.Text = "Materialele din contract:";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(145, 143);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(197, 22);
            this.txtAdresa.TabIndex = 4;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(145, 209);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(197, 22);
            this.txtEmail.TabIndex = 8;
            // 
            // btnSterge1
            // 
            this.btnSterge1.AutoSize = true;
            this.btnSterge1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSterge1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSterge1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.btnSterge1.Location = new System.Drawing.Point(17, 341);
            this.btnSterge1.Margin = new System.Windows.Forms.Padding(0);
            this.btnSterge1.Name = "btnSterge1";
            this.btnSterge1.Size = new System.Drawing.Size(33, 26);
            this.btnSterge1.TabIndex = 12;
            this.btnSterge1.Text = "🗑";
            this.ttDelete.SetToolTip(this.btnSterge1, "Sterge linia.");
            this.btnSterge1.UseVisualStyleBackColor = true;
            this.btnSterge1.Click += new System.EventHandler(this.BtnSterge1_Click);
            // 
            // txtMaterial1
            // 
            this.txtMaterial1.Location = new System.Drawing.Point(56, 343);
            this.txtMaterial1.Name = "txtMaterial1";
            this.txtMaterial1.Size = new System.Drawing.Size(256, 22);
            this.txtMaterial1.TabIndex = 10;
            this.txtMaterial1.TextChanged += new System.EventHandler(this.TxtMaterial1_TextChanged);
            // 
            // btnNewLine
            // 
            this.btnNewLine.AutoSize = true;
            this.btnNewLine.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNewLine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewLine.Enabled = false;
            this.btnNewLine.Font = new System.Drawing.Font("Stencil", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewLine.Location = new System.Drawing.Point(315, 341);
            this.btnNewLine.Margin = new System.Windows.Forms.Padding(0);
            this.btnNewLine.Name = "btnNewLine";
            this.btnNewLine.Size = new System.Drawing.Size(27, 27);
            this.btnNewLine.TabIndex = 11;
            this.btnNewLine.Text = "+";
            this.ttAdd.SetToolTip(this.btnNewLine, "Adauga o linie");
            this.btnNewLine.UseVisualStyleBackColor = true;
            this.btnNewLine.Click += new System.EventHandler(this.BtnNewLine_Click);
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.btnSave.Location = new System.Drawing.Point(17, 254);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(92, 33);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "💾 Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Underline);
            this.lblTitle.Location = new System.Drawing.Point(11, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(207, 31);
            this.lblTitle.TabIndex = 14;
            this.lblTitle.Text = "Contract numarul";
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(145, 176);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(197, 22);
            this.txtTelefon.TabIndex = 6;
            // 
            // ttDelete
            // 
            this.ttDelete.AutoPopDelay = 5000;
            this.ttDelete.InitialDelay = 10;
            this.ttDelete.ReshowDelay = 100;
            // 
            // ttAdd
            // 
            this.ttAdd.AutoPopDelay = 5000;
            this.ttAdd.InitialDelay = 10;
            this.ttAdd.ReshowDelay = 100;
            // 
            // Form_Add_Contract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(367, 588);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNewLine);
            this.Controls.Add(this.txtMaterial1);
            this.Controls.Add(this.btnSterge1);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.lblMateriale);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.lblAdresa);
            this.Controls.Add(this.txtFurnizor);
            this.Controls.Add(this.lblFurnizor);
            this.Controls.Add(this.f2Titlu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form_Add_Contract";
            this.Text = "addContractForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label f2Titlu;
        private System.Windows.Forms.Label lblFurnizor;
        private System.Windows.Forms.TextBox txtFurnizor;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblMateriale;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btnSterge1;
        private System.Windows.Forms.TextBox txtMaterial1;
        private System.Windows.Forms.Button btnNewLine;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblTitle;
        private TextBoxInt txtTelefon;
        private System.Windows.Forms.ToolTip ttDelete;
        private System.Windows.Forms.ToolTip ttAdd;
    }
}