﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project___Activitatea_de_aprovizionare
{
    public partial class Form_Afiseaza_Contracte : Form
    {
        List<Contracte> auxListContracte = new List<Contracte>();

        public Form_Afiseaza_Contracte(List<Contracte> pListContracte)
        {
            InitializeComponent();
            auxListContracte = pListContracte;
            AddVL(auxListContracte);
        }
        private void AddVL(List<Contracte> auxListContracte)
        {
            for (int i = 0; i < auxListContracte.Count; i++)
            {
                ListViewItem listItem = new ListViewItem(auxListContracte[i].nrContract.ToString());
                listItem.SubItems.Add(new ListViewItem.ListViewSubItem(listItem, auxListContracte[i].furnizor.numeFurnizor));
                listItem.SubItems.Add(new ListViewItem.ListViewSubItem(listItem, auxListContracte[i].dataContract.ToString()));
                listItem.Tag = auxListContracte[i];
                lvContracte.Items.Add(listItem);
            }
        }
    }
}